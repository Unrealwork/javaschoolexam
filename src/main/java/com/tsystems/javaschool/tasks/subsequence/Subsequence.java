package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

public class Subsequence {

  /**
   * Checks if it is possible to get a sequence which is equal to the first
   * one by removing some elements from the second one.
   *
   * @param x first sequence
   * @param y second sequence
   * @return <code>true</code> if possible, otherwise <code>false</code>
   */
  @SuppressWarnings("rawtypes")
  public boolean find(List x, List y) {
    if (x == null || y == null) {
      throw new IllegalArgumentException();
    }
    ListIterator firstIt = x.listIterator();
    ListIterator secondIt = y.listIterator();
    while (firstIt.hasNext() && secondIt.hasNext()) {
      Object firstValue = firstIt.next();
      Object secondValue = secondIt.next();
      if (!Objects.equals(firstValue, secondValue)) {
        firstIt.previous();
      }
    }
    return !firstIt.hasNext();
  }
}
