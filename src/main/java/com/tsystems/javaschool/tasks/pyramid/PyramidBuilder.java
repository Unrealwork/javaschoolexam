package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class PyramidBuilder {

  /**
   * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
   * from left to right). All vacant positions in the array are zeros.
   *
   * @param inputNumbers to be used in the pyramid
   * @return 2d array with pyramid inside
   * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
   */
  public int[][] buildPyramid(List<Integer> inputNumbers)
      throws CannotBuildPyramidException {
    if (inputNumbers == null || inputNumbers.contains(null)) {
      throw new CannotBuildPyramidException();
    }
    Optional<Integer> heightOptional = height(inputNumbers.size());
    if (heightOptional.isPresent()) {

      Collections.sort(inputNumbers);
      int height = heightOptional.get();
      int width = height * 2 - 1;
      return buildPyramid(height, width, inputNumbers);
    }
    throw new CannotBuildPyramidException();
  }

  private int[][] buildPyramid(int height, int width,
      List<Integer> sortedList) {
    int[][] pyramid = new int[height][width];
    int collectionIndex = 0;
    for (int i = 0; i < height; i++) {
      int offset = (width - ((i + 1) * 2 - 1)) / 2;
      for (int j = 0; j < width; j++) {
        if (j < offset) {
          pyramid[i][j] = 0;
        } else if (width - j < offset) {
          pyramid[i][j] = 0;
        } else {
          if ((j - offset) % 2 == 0) {
            pyramid[i][j] = sortedList.get(collectionIndex);
            collectionIndex++;
          } else {
            pyramid[i][j] = 0;
          }
        }
      }
    }
    return pyramid;
  }

  private Optional<Integer> height(long size) {
    long acc = 0;
    int i = 1;
    while (acc < size) {
      acc += i;
      i++;
    }
    return (size == acc) ? Optional.of(i - 1) : Optional.empty();
  }


}
