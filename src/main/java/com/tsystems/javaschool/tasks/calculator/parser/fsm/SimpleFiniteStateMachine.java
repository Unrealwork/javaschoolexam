package com.tsystems.javaschool.tasks.calculator.parser.fsm;

import com.tsystems.javaschool.tasks.calculator.parser.states.NoSuchTransitionException;
import com.tsystems.javaschool.tasks.calculator.parser.states.State;
import com.tsystems.javaschool.tasks.calculator.parser.transitions.Transition;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class SimpleFiniteStateMachine<T> implements FiniteSM<T> {

  private State<T> current;
  private Map<State<T>, Set<Transition<T>>> transitionMap = new HashMap<>();
  private Optional<State<T>> previousState;

  SimpleFiniteStateMachine(State<T> initialState) {
    this.current = initialState;
    this.previousState = Optional.empty();
  }

  @Override
  public State<T> currentState() {
    return current;
  }


  /**
   * Switch state of determine finite automata by passing next data object.
   *
   * @param data data object.
   */
  public FiniteSM<T> switchState(final T data) {
    State<T> nextState = transitionMap.get(current).stream()
        .filter(t -> t.isPossible(data))
        .map(Transition::transit)
        .findAny()
        .orElseThrow(NoSuchTransitionException::new);
    previousState = Optional.of(current);
    current = nextState;

    return this;
  }

  @Override
  public Optional<State<T>> previousState() {
    return previousState;
  }

  void addState(State state) {
    this.transitionMap.put(state, new HashSet<>());
  }

  FiniteSM<T> registerTransition(Transition<T> transition) {
    if (transitionMap.containsKey(transition.startState())) {
      transitionMap.get(transition.startState()).add(transition);
    } else {
      throw new IllegalStateException();
    }
    return this;
  }

  public boolean canStop() {
    return this.current.isFinal();
  }

  public static void main(String[] args) {

  }
}
