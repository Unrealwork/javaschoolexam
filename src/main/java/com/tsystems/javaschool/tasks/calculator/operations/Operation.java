package com.tsystems.javaschool.tasks.calculator.operations;

import java.math.BigDecimal;
import java.util.function.BiFunction;


public class Operation {

  private final BiFunction<BigDecimal, BigDecimal, BigDecimal> action;
  private final Integer priority;

  private Operation(BiFunction<BigDecimal, BigDecimal, BigDecimal> operation,
      Integer priority) {
    this.action = operation;
    this.priority = priority;
  }

  public static Operation of(
      BiFunction<BigDecimal, BigDecimal, BigDecimal> function,
      Integer priority) {
    return new Operation(function, priority);
  }

  public Integer getPriority() {
    return priority;
  }


  public BiFunction<BigDecimal, BigDecimal, BigDecimal> getAction() {
    return action;
  }
}
