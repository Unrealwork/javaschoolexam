package com.tsystems.javaschool.tasks.calculator.parser.states;

public interface State<T> {

  boolean isFinal();

  boolean canStartWith(T value);
}
