package com.tsystems.javaschool.tasks.calculator.parser.fsm;

import com.tsystems.javaschool.tasks.calculator.parser.states.State;
import com.tsystems.javaschool.tasks.calculator.parser.transitions.Transition;
import com.tsystems.javaschool.tasks.calculator.parser.transitions.TransitionBuilder;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;

public class FiniteSMBuilder<T> {

  private SimpleFiniteStateMachine<T> instance;

  public FiniteSMBuilder(State initialState, Set<State> states) {
    this.instance = new SimpleFiniteStateMachine<>(initialState);
    states.forEach(s -> instance.addState(s));
  }

  public FiniteSMBuilder(State initialState, State... states) {
    this(initialState, new HashSet<>(Arrays.asList(states)));
  }

  public FiniteSMBuilder<T> withTransitionSet(State<T> startStates,
      Set<State<T>> endStates) {
    endStates.forEach(endState -> withTransition(startStates, endState));
    return this;
  }

  public FiniteSMBuilder<T> withTransitionSet(State<T> startState,
      State<T>... endStates) {
    return withTransitionSet(startState,
        new HashSet<>(Arrays.asList(endStates)));
  }

  public FiniteSMBuilder<T> withTransition(State<T> startState, State<T> endState) {
    Transition<T> transition = new TransitionBuilder<>(startState, endState)
        .build();
    this.instance.registerTransition(transition);
    return this;
  }

  public FiniteSMBuilder<T> withTransition(State<T> startState, State<T> endState,
      Predicate<T> predicate) {
    Transition<T> transition = new TransitionBuilder<>(startState, endState)
        .transitionPredicate(predicate)
        .build();
    this.instance.registerTransition(transition);
    return this;
  }

  public FiniteSM<T> build() {
    return this.instance;
  }
}
