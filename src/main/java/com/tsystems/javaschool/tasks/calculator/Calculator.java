package com.tsystems.javaschool.tasks.calculator;

import com.tsystems.javaschool.tasks.calculator.calculations.RpnEvaluator;
import com.tsystems.javaschool.tasks.calculator.calculations.ShuntingYardCollector;
import com.tsystems.javaschool.tasks.calculator.parser.ExpressionParser;
import com.tsystems.javaschool.tasks.calculator.parser.tokens.Token;
import java.math.RoundingMode;

public class Calculator {

  /**
   * Evaluate statement represented as string.
   *
   * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
   * parentheses, operations signs '+', '-', '*', '/'<br>
   * Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
   * @return string value containing result of evaluation or null if statement is invalid
   */
  public String evaluate(String statement) {
    try {
      Iterable<Token> rpn = ExpressionParser
          .tokens(statement)
          .collect(ShuntingYardCollector.toRpn());
      return RpnEvaluator.evaluate(rpn)
          .map(num -> num.setScale(4, RoundingMode.HALF_UP)
              .stripTrailingZeros()
              .toPlainString())
          .orElse(null);
    } catch (Exception e) {
      return null;
    }
  }

}
