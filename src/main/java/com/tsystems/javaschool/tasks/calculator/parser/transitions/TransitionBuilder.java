package com.tsystems.javaschool.tasks.calculator.parser.transitions;

import com.tsystems.javaschool.tasks.calculator.parser.states.State;
import java.util.Optional;
import java.util.function.Predicate;

public class TransitionBuilder<T> {

  private static class TransitionImpl<T> implements Transition<T> {

    private final State<T> finalState;
    private Optional<Predicate<T>> transitPredicate;
    private final State<T> initialState;

    private TransitionImpl(State<T> startState, State<T> finalState) {
      this.initialState = startState;
      this.finalState = finalState;
      this.transitPredicate = Optional.empty();
    }

    @Override
    public State<T> startState() {
      return initialState;
    }

    @Override
    public boolean isPossible(T value) {
      return getTransitPredicate().test(value);
    }

    @Override
    public State transit() {
      return finalState;
    }

    @Override
    public State<T> state(T value) {
      return finalState;
    }

    public Predicate<T> getTransitPredicate() {
      return transitPredicate.orElse(finalState::canStartWith);
    }

    public void setTransitPredicate(Predicate<T> transitPredicate) {
      this.transitPredicate = Optional.of(transitPredicate);
    }
  }

  private TransitionImpl<T> instance;

  public TransitionBuilder(State<T> state, State<T> destinationState) {
    instance = new TransitionImpl<T>(state, destinationState);
  }


  public TransitionBuilder<T> transitionPredicate(Predicate<T> predicate) {
    this.instance.setTransitPredicate(predicate);
    return this;
  }

  public Transition<T> build() {
    return this.instance;
  }
}
