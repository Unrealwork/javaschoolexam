package com.tsystems.javaschool.tasks.calculator.parser.fsm;

import com.tsystems.javaschool.tasks.calculator.parser.states.State;
import java.util.Optional;

public interface FiniteSM<T> {

  State<T> currentState();

  FiniteSM<T> switchState(final T data);

  Optional<State<T>> previousState();

  boolean canStop();
}
