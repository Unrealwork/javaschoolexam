package com.tsystems.javaschool.tasks.calculator.parser;

import com.tsystems.javaschool.tasks.calculator.parser.states.State;
import java.util.function.Predicate;
import java.util.stream.Stream;

public enum ExpressionState implements State<Character> {

  INITIAL(true, c -> true),
  EMPTY(true, Character::isWhitespace),
  NUMBER(true, c -> c.equals('-') || Character.isDigit(c)),
  OPERATION(false, c -> Stream.of('+', '-', '/', '*').anyMatch(c::equals)),
  CLOSING_BRACKET(true, Character.valueOf(')')::equals),
  OPEN_BRACKET(false, Character.valueOf('(')::equals);


  private final boolean isFinal;
  private final Predicate<Character> predicate;

  ExpressionState(boolean isFinal, Predicate<Character> predicate) {
    this.isFinal = isFinal;
    this.predicate = predicate;
  }


  @Override
  public boolean isFinal() {
    return isFinal;
  }

  @Override
  public boolean canStartWith(Character symbol) {
    return predicate.test(symbol);
  }
}
