package com.tsystems.javaschool.tasks.calculator.calculations;

import com.tsystems.javaschool.tasks.calculator.operations.Operation;
import com.tsystems.javaschool.tasks.calculator.operations.OperationFactory;
import com.tsystems.javaschool.tasks.calculator.parser.tokens.Token;
import com.tsystems.javaschool.tasks.calculator.parser.tokens.TokenCategory;
import java.math.BigDecimal;
import java.util.Optional;
import java.util.Stack;

public class RpnEvaluator {

  /**
   * Evaluate arithmetic expression represented in RPN.
   *
   * @param rpn Iterable<Token> that represents RPN.
   */
  public static Optional<BigDecimal> evaluate(Iterable<Token> rpn) {
    Stack<BigDecimal> calculationStack = new Stack<>();
    try {
      for (Token t : rpn) {
        if (t.getType().equals(TokenCategory.NUMBER)) {
          calculationStack.push(new BigDecimal(t.getValue()));
        } else if (t.getType().equals(TokenCategory.OPERATION)) {
          BigDecimal rightOperand = calculationStack.pop();
          BigDecimal leftOperand = calculationStack.pop();
          Operation operation = OperationFactory.getOperation(t.getValue());
          calculationStack
              .push(operation.getAction().apply(leftOperand, rightOperand));
        } else {
          throw new IllegalStateException();
        }
      }
    } catch (Exception e) {
      return Optional.empty();
    }
    if (calculationStack.size() == 1) {
      return Optional.of(calculationStack.pop());
    }
    throw new IllegalStateException();
  }
}
