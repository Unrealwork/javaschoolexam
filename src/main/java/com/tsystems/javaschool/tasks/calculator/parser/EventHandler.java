package com.tsystems.javaschool.tasks.calculator.parser;

import com.tsystems.javaschool.tasks.calculator.parser.states.State;

@FunctionalInterface
public interface EventHandler {

  void handle(State state);
}
