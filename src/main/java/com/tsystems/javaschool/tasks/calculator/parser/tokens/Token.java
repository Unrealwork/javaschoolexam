package com.tsystems.javaschool.tasks.calculator.parser.tokens;

public interface Token {

  String getValue();

  TokenCategory getType();
}
