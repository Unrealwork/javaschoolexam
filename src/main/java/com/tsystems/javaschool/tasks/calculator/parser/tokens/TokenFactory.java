package com.tsystems.javaschool.tasks.calculator.parser.tokens;

import java.util.stream.Stream;

public class TokenFactory {

  /**
   * Retrieve operation token by symbol.
   *
   * @param value symbol
   */
  public static Token operation(CharSequence value) {
    if (Stream.of("+", "-", "*", "/").anyMatch(s -> s.contentEquals(value))) {
      return new TokenImpl(value, TokenCategory.OPERATION);
    }
    throw new IllegalTokenException();
  }

  public static Token number(CharSequence value) {
    return new TokenImpl(value, TokenCategory.NUMBER);
  }

  private static class TokenImpl implements Token {

    private TokenCategory category;

    @Override
    public String toString() {
      return "TokenImpl{"
          + "category=" + category
          + ", value='" + value + '\''
          + '}';
    }

    private String value;

    private TokenImpl(
        CharSequence value, TokenCategory category) {
      this.category = category;
      this.value = value.toString();
    }


    @Override
    public String getValue() {
      return value;
    }

    @Override
    public TokenCategory getType() {
      return category;
    }
  }
}
