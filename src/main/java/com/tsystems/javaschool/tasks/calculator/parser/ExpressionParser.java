package com.tsystems.javaschool.tasks.calculator.parser;

import static com.tsystems.javaschool.tasks.calculator.parser.ExpressionState.CLOSING_BRACKET;
import static com.tsystems.javaschool.tasks.calculator.parser.ExpressionState.EMPTY;
import static com.tsystems.javaschool.tasks.calculator.parser.ExpressionState.INITIAL;
import static com.tsystems.javaschool.tasks.calculator.parser.ExpressionState.NUMBER;
import static com.tsystems.javaschool.tasks.calculator.parser.ExpressionState.OPEN_BRACKET;
import static com.tsystems.javaschool.tasks.calculator.parser.ExpressionState.OPERATION;
import static com.tsystems.javaschool.tasks.calculator.parser.ExpressionState.values;

import com.tsystems.javaschool.tasks.calculator.parser.fsm.FiniteSM;
import com.tsystems.javaschool.tasks.calculator.parser.fsm.FiniteSMBuilder;
import com.tsystems.javaschool.tasks.calculator.parser.states.NoSuchTransitionException;
import com.tsystems.javaschool.tasks.calculator.parser.states.State;
import com.tsystems.javaschool.tasks.calculator.parser.tokens.SymbolToken;
import com.tsystems.javaschool.tasks.calculator.parser.tokens.Token;
import com.tsystems.javaschool.tasks.calculator.parser.tokens.TokenFactory;
import com.tsystems.javaschool.tasks.calculator.utils.StreamUtils;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Stream;

public class ExpressionParser {

  private ExpressionParser() {
    this.buffer = new StringBuffer();
  }

  private final StringBuffer buffer;


  private static final Map<State<Character>, Function<CharSequence, Token>> STATE_TOKEN_DICT = Collections
      .unmodifiableMap(
          new HashMap<ExpressionState, Function<CharSequence, Token>>() {{
            put(NUMBER, TokenFactory::number);
            put(OPERATION, TokenFactory::operation);
            put(CLOSING_BRACKET, s -> SymbolToken.CLOSING_BRACKET);
            put(OPEN_BRACKET, s -> SymbolToken.OPEN_BRACKET);
          }});


  public static Stream<Token> tokens(String statement)
      throws NoSuchTransitionException {
    return new ExpressionParser().transform(statement);
  }

  private Stream<Token> transform(String statement) {
    final FiniteSM<Character> fsm = initialStateMachine();
    final AtomicInteger i = new AtomicInteger(0);
    return statement.chars()
        .mapToObj(c -> ((char) c)).flatMap(c -> {
          i.incrementAndGet();
          fsm.switchState(c);
          if (fsm.previousState().isPresent()) {
            State<Character> previousState = fsm.previousState().get();
            Optional<Token> token = Optional.empty();
            if (!previousState.equals(fsm.currentState())) {
              token = extractToken(previousState);
              buffer.setLength(0);
            }
            buffer.append(c);
            if (i.get() == statement.length()) {
              if (fsm.canStop()) {
                return StreamUtils
                    .ofOptionals(token, extractToken(fsm.currentState()));
              } else {
                throw new IllegalStateException();
              }
            }
            return StreamUtils.ofOptionals(token);
          } else {
            return Stream.empty();
          }
        });
  }


  private Optional<Token> extractToken(
      State<Character> state) {
    if (STATE_TOKEN_DICT.containsKey(state)) {
      Token token = STATE_TOKEN_DICT.get(state).apply(buffer);
      return Optional.of(token);
    }
    return Optional.empty();
  }

  private FiniteSM<Character> initialStateMachine() {
    return new FiniteSMBuilder<Character>(INITIAL,
        values())
        .withTransitionSet(INITIAL, OPEN_BRACKET, EMPTY, NUMBER)
        .withTransitionSet(OPEN_BRACKET, EMPTY, NUMBER,
            OPEN_BRACKET)
        .withTransitionSet(NUMBER, OPERATION, CLOSING_BRACKET,
            EMPTY)
        .withTransitionSet(CLOSING_BRACKET, OPERATION, EMPTY)
        .withTransitionSet(OPERATION, EMPTY, OPEN_BRACKET)
        .withTransitionSet(EMPTY, EMPTY, OPERATION, NUMBER,
            OPEN_BRACKET,
            CLOSING_BRACKET)
        .withTransition(OPERATION, NUMBER, Character::isDigit)
        .withTransition(NUMBER, NUMBER, this::numberValidation)
        .build();
  }

  private boolean numberValidation(Character c) {
    if (buffer.length() > 0) {
      if (buffer.indexOf(".") > 0) {
        return Character.isDigit(c);
      } else {
        return Character.isDigit(c) || c.equals('.');
      }
    } else {
      return NUMBER.canStartWith(c);
    }
  }
}
