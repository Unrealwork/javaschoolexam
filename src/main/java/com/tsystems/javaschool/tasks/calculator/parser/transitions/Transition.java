package com.tsystems.javaschool.tasks.calculator.parser.transitions;

import com.tsystems.javaschool.tasks.calculator.parser.states.State;

public interface Transition<T> {

  State<T> startState();

  boolean isPossible(T value);

  State<T> transit();

  State<T> state(T value);
}
