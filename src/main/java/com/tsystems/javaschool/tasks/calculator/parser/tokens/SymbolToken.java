package com.tsystems.javaschool.tasks.calculator.parser.tokens;

public enum SymbolToken implements Token {
  OPEN_BRACKET("(", TokenCategory.OPEN_BRACKET),
  CLOSING_BRACKET(")", TokenCategory.CLOSING_BRACKET);

  private TokenCategory type;
  private String value;

  SymbolToken(String value, TokenCategory type) {
    this.value = value;
    this.type = type;
  }

  @Override
  public String getValue() {
    return this.value;
  }

  @Override
  public TokenCategory getType() {
    return this.type;
  }
}
