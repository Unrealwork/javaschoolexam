package com.tsystems.javaschool.tasks.calculator.parser.tokens;

public enum TokenCategory {
  OPEN_BRACKET, NUMBER, WHITESPACE, CLOSING_BRACKET, OPERATION
}
