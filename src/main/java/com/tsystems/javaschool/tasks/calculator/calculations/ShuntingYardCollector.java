package com.tsystems.javaschool.tasks.calculator.calculations;

import com.tsystems.javaschool.tasks.calculator.operations.Operation;
import com.tsystems.javaschool.tasks.calculator.operations.OperationFactory;
import com.tsystems.javaschool.tasks.calculator.parser.tokens.Token;
import com.tsystems.javaschool.tasks.calculator.parser.tokens.TokenCategory;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

public class ShuntingYardCollector implements
    Collector<Token, Queue<Token>, Queue<Token>> {

  private Stack<Token> operatorsStack = new Stack<>();

  private ShuntingYardCollector() {

  }

  @Override
  public Supplier<Queue<Token>> supplier() {
    return ArrayDeque::new;
  }

  @Override
  public BiConsumer<Queue<Token>, Token> accumulator() {
    return (queue, token) -> {
      TokenCategory type = token.getType();
      if (type.equals(TokenCategory.NUMBER)) {
        queue.add(token);
      } else if (type.equals(TokenCategory.OPERATION)) {
        Operation currentOperation = OperationFactory
            .getOperation(token.getValue());
        while (!operatorsStack.empty()) {
          Token lastOperationToken = operatorsStack.peek();
          TokenCategory lastOperationType = lastOperationToken.getType();
          if (lastOperationType.equals(TokenCategory.OPERATION)) {
            Operation lastOperation = OperationFactory
                .getOperation(lastOperationToken.getValue());
            if (lastOperation.getPriority() >= currentOperation.getPriority()) {
              queue.add(operatorsStack.pop());
            } else {
              break;
            }
          } else {
            break;
          }
        }
        operatorsStack.push(token);
      } else if (type.equals(TokenCategory.OPEN_BRACKET)) {
        operatorsStack.push(token);
      } else if (type.equals(TokenCategory.CLOSING_BRACKET)) {
        Token lastOperatorToken;
        do {
          lastOperatorToken = operatorsStack.pop();
          if (!lastOperatorToken.getType().equals(TokenCategory.OPEN_BRACKET)) {
            queue.add(lastOperatorToken);
          }
        } while (!lastOperatorToken.getType()
            .equals(TokenCategory.OPEN_BRACKET));
      }
    };
  }

  @Override
  public BinaryOperator<Queue<Token>> combiner() {
    return (q1, q2) -> q1;
  }

  @Override
  public Function<Queue<Token>, Queue<Token>> finisher() {

    return q -> {
      while (!operatorsStack.empty()) {
        q.add(operatorsStack.pop());
      }
      return q;
    };
  }

  @Override
  public Set<Characteristics> characteristics() {
    return Collections.emptySet();
  }

  /**
   * Create collector that reduce <code>Stream<Token></code>
   * to reverse Polish notation.
   *
   * @return Queue with tokens that represents RPN.
   */
  public static ShuntingYardCollector toRpn() {
    return new ShuntingYardCollector();
  }
}
