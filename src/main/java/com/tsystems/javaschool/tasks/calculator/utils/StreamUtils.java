package com.tsystems.javaschool.tasks.calculator.utils;

import java.util.Optional;
import java.util.stream.Stream;

public class StreamUtils {

  public static <T> Stream<T> ofOptionals(Optional<T>... tokens) {
    return Stream.of(tokens)
        .flatMap(op -> op.map(Stream::of).orElseGet(Stream::empty));
  }
}
