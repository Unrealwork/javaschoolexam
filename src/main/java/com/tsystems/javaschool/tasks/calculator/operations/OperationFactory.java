package com.tsystems.javaschool.tasks.calculator.operations;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class OperationFactory {

  private static final Map<String, Operation> OPERATIONS_DICT = Collections
      .unmodifiableMap(
          new HashMap<String, Operation>()

          {
            {
              put("+", Operation.of(BigDecimal::add, 0));
              put("-", Operation.of(BigDecimal::subtract, 0));
              put("*", Operation.of(BigDecimal::multiply, 1));
              put("/",
                  Operation
                      .of((a, b) -> a.divide(b, MathContext.DECIMAL128), 1));
            }
          });


  public static Operation getOperation(String operationSymbol) {
    if (OPERATIONS_DICT.containsKey(operationSymbol)) {
      return OPERATIONS_DICT.get(operationSymbol);
    }
    throw new IllegalArgumentException(
        String.format("%s is not supported operation", operationSymbol));
  }
}
